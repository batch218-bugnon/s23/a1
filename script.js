
// ACTIVITY 1

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Snorlax", "Charmander"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
		
	}
}
console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);

trainer.talk();



// ACTIVITY 2

function pokemon(name, level){
	this.name = name;
    this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        
        target.health -= this.attack 
        console.log(target.name + " health is not reduced to " + target.health);

        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = function(){
        console.log(this.name  + "has fainted")
    }
}		


let pikachu = new pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new pokemon("Mewtwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);













